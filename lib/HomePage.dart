import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:web3modal_flutter/web3modal_flutter.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? _selectedAddress;

  // Define a getter for selectedAddress
  String? get selectedAddress => _selectedAddress;

  late DeployedContract deployedContract;
  late W3MService _w3mService;
  bool _isLoading = true;
  bool _isError = false;
  String? _abi;

  @override
  void initState() {
    super.initState();
    _initializeW3MService();
  }

  void _initializeW3MService() async {
    try {
      // W3MChainPresets.chains.putIfAbsent(_chainId, () => _smartChain);
      _w3mService = W3MService(
        projectId: 'dc7fa0f30a053e2d3f38386d33c137eb',
        metadata: const PairingMetadata(
          name: 'Web3Modal Flutter Example',
          description: 'Web3Modal Flutter Example',
          url: 'https://www.walletconnect.com/',
          icons: ['https://walletconnect.com/walletconnect-logo.png'],
          redirect: Redirect(
            native: 'w3m://',
            universal: 'https://www.walletconnect.com',
          ),
        ),
      );

      await _w3mService.init();
      setState(() {
        _isLoading = false;
      });
      await _fetchContractABI('0xc166a8dd8e48355774ac95933f746b57a724a464');
    } catch (e) {
      debugPrint('Error initializing W3MService: $e');
      setState(() {
        _isLoading = false;
        _isError = true;
      });
      deployedContract = DeployedContract(
        ContractAbi.fromJson(jsonDecode(_abi!), 'GURUToken'),
        EthereumAddress.fromHex("0xc166a8dd8e48355774ac95933f746b57a724a464"),
      );
    }
  }

  Future<void> _fetchContractABI(String contractAddress) async {
    const apiKey = 'Q6GD5QU1167PJF1ETKGRIF3BAGIRN65RXU';
    final url =
        'https://api.bscscan.com/api?module=contract&action=getabi&address=$contractAddress&apikey=$apiKey';

    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final Map<String, dynamic> data = json.decode(response.body);
      if (data['status'] == '1') {
        setState(() {
          _abi = data['result'];
        });
      } else {
        debugPrint('Failed to fetch contract ABI.');
      }
    } else {
      debugPrint('HTTP Error: ${response.statusCode}');
    }
  }

  Future<void> getWalletBalance() async {
    // Get balance of wallet
    final result = await _w3mService.requestReadContract(
      deployedContract: deployedContract,
      functionName: 'balanceOf',
      rpcUrl: 'https://{rpc-url}.com',
      parameters: [
        EthereumAddress.fromHex(
            '0xc166a8dd8e48355774ac95933f746b57a724a464'), // Your address
      ],
    );
  }

  Future<void> getTotalSupply() async {
    // Get token total supply
    final result = await _w3mService.requestReadContract(
      deployedContract: deployedContract,
      functionName: 'totalSupply',
      rpcUrl: "https://bsc-dataseed.binance.org/",
    );
  }

  Future<void> transferToken() async {
    if (_w3mService.isConnected) {
      // try {
      //   final connectedAddress = _w3mService.session?.selectedAddress;
      //   if (connectedAddress != null) {
      // Transfer 0.01 amount of Token using Smart Contract's transfer function
      final result = await _w3mService.requestWriteContract(
        topic: _w3mService.session?.topic ?? 'traaaaaaaaa',
        chainId: _chainId,
        rpcUrl: 'https://{rpc-url}.com',
        deployedContract: deployedContract,
        functionName: 'transfer',
        transaction: Transaction(
          from: EthereumAddress.fromHex(
              "0x1Efdc9C8B4b6723B6D741B95fd0DD65E30805C4F"),
          to: EthereumAddress.fromHex(
              '0x756A6144Bd8Dd9d9029A3069620AF146F6De39fB'),
          value: EtherAmount.fromUnitAndValue(EtherUnit.finney, 10),
        ),
      );

      debugPrint('Transfer Result: $result');
      // } else {
      //   debugPrint('No connected account.');
      // }
      // } catch (e) {
      //   debugPrint('Transfer Error: $e');
      // }
    } else {
      debugPrint('Web3Modal is not connected.');
    }
  }

  void _onPersonalSign() async {
    if (_w3mService.isConnected) {
      try {
        await _w3mService.launchConnectedWallet();
        final result = await _w3mService.request(
          topic: _w3mService.session?.topic ?? '',
          chainId: _chainId,
          request: const SessionRequestParams(
            method: 'personal_sign',
            params: ['Sign this', 'GURUToken'],
          ),
        );
        debugPrint('Personal Sign Result: $result');
      } catch (e) {
        debugPrint('Personal Sign Error: $e');
      }
    } else {
      debugPrint('Web3Modal is not connected.');
    }
  }

  Future<void> writeMessage() async {
    // Write a message data
    final result = await _w3mService.requestWriteContract(
      topic: _w3mService.session?.topic ?? 'traaaaaaa',
      chainId: _chainId,
      rpcUrl: 'https://{rpc-url}.com',
      deployedContract: deployedContract,
      functionName: 'sayHello',
      transaction: Transaction(
        from: EthereumAddress.fromHex(
            '0x1Efdc9C8B4b6723B6D741B95fd0DD65E30805C4F'),
      ),
      parameters: ['Hello world!'],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
      ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : _isError
              ? const Center(
                  child: Text('Error initializing W3MService'),
                )
              : Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // if (!_w3mService.isConnected) ...[
                      W3MNetworkSelectButton(service: _w3mService),
                      const SizedBox(height: 10),
                      W3MConnectWalletButton(service: _w3mService),
                      const SizedBox(height: 10),
                      // ] else ...[
                      W3MAccountButton(service: _w3mService),
                      // ],
                      const SizedBox(height: 10),
                      ElevatedButton(
                        onPressed: _abi != null ? _onPersonalSign : null,
                        child: const Text("Personal Sign"),
                      ),
                    ],
                  ),
                ),
    );
  }
}

const _chainId = "56";

// final _smartChain = W3MChainInfo(
//     chainName: "SmartChain",
//     chainId: _chainId,
//     namespace: "56:$_chainId",
//     tokenName: "BNB",
//     rpcUrl: "https://bsc-dataseed.binance.org/",
//     blockExplorer:
//         W3MBlockExplorer(name: "BNB Explorer", url: "https://bscscan.com"));
